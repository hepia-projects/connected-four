"use strict";

let initialState;
let states = [];

function select(){

}

function simulate(state){

}

function backPropagate
function play1Move(){
    let state = select()
    let res = simulate(state);
    backPropagate(state, res);
}

function play(connect4Game){
    initialState = connect4Game.serializeBoard();
    for(let i = 0;i<1000;i++){
        play1Move()
    }

    return bestMostVisitedState();
}

module.exports = {
    play: play,
};